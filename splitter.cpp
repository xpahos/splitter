#include "main.h"
#include "file_dumper.h"
#include "yt_dumper.h"

#include <contrib/libs/libpcap/pcap-int.h>
#include <library/digest/murmur/murmur.h>

#include <cstring>
#include <cerrno>
#include <signal.h>
#include <util/folder/path.h>


std::atomic_bool CAPTURE_ENABLED{true};

static void ExitHandler(int signum);
static void SetExitHandler();

int RunFileSplitter(const TString& filePath,
                   const TString& outputDir,
                   int depth) {
    SetExitHandler();

    std::unique_ptr<TClientWorker<TFileDumper>> clientWorker(
        TClientWorker<TFileDumper>::CreateWorker(filePath, outputDir, "", depth));
    if (clientWorker) {
        clientWorker->Start();
    } else {
        return 1;
    }

    return 0;
}

class TYtPacketsReducer: public NYT::IReducer<NYT::TTableReader<NYT::TNode>, NYT::TTableWriter<NYT::TNode>> {
public:
    void Do(TReader* reader, TWriter* writer) override {
        TStringStream data;

        for (; reader->IsValid(); reader->Next()) {
            auto& row = reader->GetRow();
            data << row["data"].AsString();
        }

        NYT::TNode result;
        result["data"] = data.Str();
        writer->AddRow(result);
    }
};
REGISTER_REDUCER(TYtPacketsReducer);

int RunYtSplitter(const TString& filePath, const TString& clusterName, const TString& tableName, bool append) {
    SetExitHandler();

    auto ytClient = NYT::CreateClient(clusterName);

    if(ytClient->Exists(tableName) and !append) {
        Cerr << STRINGBUF("Table [") << tableName << STRINGBUF("] already exists\n");
        return 1;
    }

    auto ytTrx = ytClient->StartTransaction();

    std::unique_ptr<TClientWorker<TYtDumper>> clientWorker(
        TClientWorker<TYtDumper>::CreateWorker(filePath, "", ytTrx, tableName));
    if (clientWorker) {
        clientWorker->Start();
        ytTrx->Sort(
            NYT::TSortOperationSpec()
                .SortBy({"hash", "order"})
                .AddInput(tableName)
                .Output(tableName)
            );

        ytTrx->Commit();

    } else {
        ytTrx->Abort();
        return 1;
    }

    return 0;
}


template<typename T>
std::unique_ptr<TClientWorker<T>>
TClientWorker<T>::CreateWorker(const TString& filePath, const TString& outputDir, const TString& filter, int depth) {
    std::unique_ptr<TPcap> pcap = std::make_unique<TPcap>(filePath);
    if (!pcap->IsInit()) {
        Cerr << pcap->ErrorString();
        return nullptr;
    }

    std::unique_ptr<TPcapFilter> pcapFilter = std::make_unique<TPcapFilter>(pcap->Session(), filter);
    if (!pcapFilter->IsInit()) {
        Cerr << pcapFilter->ErrorString();
        return nullptr;
    }

    std::unique_ptr<T> dumperInstance = std::make_unique<T>(pcap->LinkType(), pcap->SnapSize());

    std::unique_ptr<TClientWorker<T>> instance(new TClientWorker<T>(std::move(pcap), std::move(pcapFilter), std::move(dumperInstance), outputDir, depth));
    return instance;
}

template<typename T>
std::unique_ptr<TClientWorker<T>>
TClientWorker<T>::CreateWorker(const TString& filePath, const TString& filter, NYT::ITransactionPtr ytClient, const TString& tableName) {
    std::unique_ptr<TPcap> pcap = std::make_unique<TPcap>(filePath);
    if (!pcap->IsInit()) {
        Cerr << pcap->ErrorString();
        return nullptr;
    }

    std::unique_ptr<TPcapFilter> pcapFilter = std::make_unique<TPcapFilter>(pcap->Session(), filter);
    if (!pcapFilter->IsInit()) {
        Cerr << pcapFilter->ErrorString();
        return nullptr;
    }

    std::unique_ptr<T> dumperInstance = std::make_unique<T>(ytClient, tableName, pcap->FileHeader());
    if (!dumperInstance->IsInit()) {
        Cerr << dumperInstance->ErrorString();
        return nullptr;
    }

    std::unique_ptr<TClientWorker<T>> instance(new TClientWorker<T>(std::move(pcap), std::move(pcapFilter), std::move(dumperInstance)));
    return instance;
}

template<typename T>
void TClientWorker<T>::Start() {
    TPcapPacketHeader *pktHdr;
    const unsigned char *pkt;

    int ret;
    while ((ret = Pcap_->NextEx(&pktHdr, &pkt)) >= 0 and CAPTURE_ENABLED.load()) {
        /* ret == 0 is timeout and pktHdr, pkt invalid pointers */
        if (ret == 0)
            usleep(100000);
        else
            ProcessPacket(pktHdr, pkt);
    }

    if (ret == -1) {
        Cerr << STRINGBUF("Failed to capture packets: ")
             << Pcap_->ErrorString() << STRINGBUF("\n");
    } else if (ret == -2) {
        Cout << STRINGBUF("Capture loop is ended\n");
    }

    DumperInstance_->Finish();
}

template<typename T>
void TClientWorker<T>::ProcessPacket(
        const TPcapPacketHeader *pktHdr,
        const unsigned char *packet)
{
    uint8_t ipVersion = (*(packet + Pcap_->Offset())) >> 4;

    unsigned char srcIp[16]{0};
    unsigned char dstIp[16]{0};

    uint8_t proto = 0x00;
    uint8_t headerSize = 0x00;

    int afInet = 0;

    if (ipVersion == IPV4) {
        auto ipLayer = (TIp4Frame *) (packet + Pcap_->Offset());

        std::memcpy(&srcIp, &ipLayer->srcIp, 4);
        std::memcpy(&dstIp, &ipLayer->dstIp, 4);
        proto = ipLayer->proto;

        /* We can't cast 4 fields in С/C++. first 4bits of ipVersion is version, second is IHL */
        headerSize = (ipLayer->ipVersion & 0x0f) * 4;
        afInet = AF_INET;
    } else if (ipVersion == IPV6) {
        auto ipLayer = (TIp6Frame *) (packet + Pcap_->Offset());
        std::memcpy(&srcIp, &ipLayer->srcIp, 16);
        std::memcpy(&dstIp, &ipLayer->dstIp, 16);
        proto = ipLayer->proto;
        afInet = AF_INET6;

        /* Fixed size of header. All optional headers stored in proto field */
        headerSize += 40;

        if (proto != IP6OPT_TCP) {
            bool nxt = true;

            while (nxt) {
                auto ipLayerNxt = (TIp6FrameNxt *)(packet + Pcap_->Offset() + headerSize);
                proto = ipLayerNxt->proto;
                /* 96 bits for optional field */
                headerSize += 12;
                /* Match only basic fields */
                switch (proto) {
                    case IP6OPT_TCP:
                    case IP6OPT_HOP:
                    case IP6OPT_ROUTING:
                    case IP6OPT_FRAG:
                    case IP6OPT_AH:
                    case IP6OPT_DST:
                    case IP6OPT_MIPV6:
                    case IP6OPT_SHIM6:
                        break;
                    default:
                        nxt = false;
                        break;
                }
            }
        }
    } else {
        return;
    }

    /* Invalid ip header */
    if ((ipVersion == IPV4 && headerSize < 20) || (ipVersion == IPV6 && headerSize < 40))
        return;

    // Match only TCP
    if (proto == IP6OPT_TCP) {
        auto tcpLayer = (TTcpFrame *)(packet + Pcap_->Offset() + headerSize);

        uint64_t hash = 0L;
        uint64_t hashInv = 0L;

        TMurmurHash2A<uint64_t> hasher;
        hasher.Update(&srcIp, 16);
        hasher.Update(&tcpLayer->srcPort, 2);
        hasher.Update(&dstIp, 16);
        hasher.Update(&tcpLayer->dstPort, 2);
        hash = hasher.Value();

        TMurmurHash2A<uint64_t> hasherInv;
        hasherInv.Update(&dstIp, 16);
        hasherInv.Update(&tcpLayer->dstPort, 2);
        hasherInv.Update(&srcIp, 16);
        hasherInv.Update(&tcpLayer->srcPort, 2);
        hashInv = hasherInv.Value();

        EvalDump(pktHdr, packet, tcpLayer, srcIp, dstIp, afInet, hash, hashInv);
    }

}

template<>
void TClientWorker<TYtDumper>::EvalDump(const TPcapPacketHeader *pktHdr, const unsigned char *packet,
                                        const TTcpFrame *frame, const unsigned char *srcIp, const unsigned char *dstIp,
                                        int afInet, uint64_t hash, uint64_t hashInv) {
    Y_UNUSED(frame);
    Y_UNUSED(srcIp);
    Y_UNUSED(dstIp);
    Y_UNUSED(afInet);

    struct pcap_sf_pkthdr sf_hdr;
    sf_hdr.ts.tv_sec  = pktHdr->ts.tv_sec;
    sf_hdr.ts.tv_usec = pktHdr->ts.tv_usec;
    sf_hdr.caplen     = pktHdr->caplen;
    sf_hdr.len        = pktHdr->len;

    /* PCAP always stores data in 32bit format */
    size_t structSize = sizeof(sf_hdr);
    std::vector<char> buf(structSize + pktHdr->caplen);
    std::memcpy(&buf[0], &sf_hdr, structSize);
    std::memcpy(&buf[0]+structSize, packet, pktHdr->caplen);

    TString str;
    std::copy(buf.begin(), buf.end(), std::back_inserter(str));

    DumperInstance_->DumpData(hash, hashInv, str);
}

template<>
void TClientWorker<TFileDumper>::EvalDump(const TPcapPacketHeader *pktHdr,
                                const unsigned char *packet,
                                const TTcpFrame *frame,
                                const unsigned char* srcIp,
                                const unsigned char* dstIp,
                                int afInet, uint64_t hash, uint64_t hashInv) {
    if (DumperInstance_->HasKey(hashInv)) {
        DumperInstance_->DumpData(hashInv, pktHdr, packet);
    } else if (DumperInstance_->HasKey(hash)) {
        DumperInstance_->DumpData(hash, pktHdr, packet);
    } else {
        char src[INET6_ADDRSTRLEN]{0};
        char dst[INET6_ADDRSTRLEN]{0};

        uint16_t srcPort = ntohs(frame->srcPort);
        uint16_t dstPort = ntohs(frame->dstPort);

        if (inet_ntop(afInet, srcIp, src, INET6_ADDRSTRLEN) == nullptr)
            return;
        if (inet_ntop(afInet, dstIp, dst, INET6_ADDRSTRLEN) == nullptr)
            return;

        TStringStream srcStreamName;
        TStringStream dstStreamName;

        srcStreamName << src << TStringBuf(".") << srcPort << TStringBuf("-") << dst << TStringBuf(".") << dstPort;
        dstStreamName << dst << TStringBuf(".") << dstPort << TStringBuf("-") << src << TStringBuf(".") << srcPort;

        TStringStream srcFileName(OutputDir_);
        TStringStream dstFileName(OutputDir_);

        if (OutputDir_[OutputDir_.size()-1] != '/') {
            srcFileName << "/";
            dstFileName << "/";
        }

        int i = 0;
        char c = 0;
        while (i < Depth_) {
            c = srcStreamName.Str()[i];
            if (std::isdigit(c) || std::isalpha(c)) {
                srcFileName << srcStreamName.Str()[i] << "/";
            }
            ++i;
        }

        i = 0;
        while (i < Depth_) {
            c = dstStreamName.Str()[i];
            if (std::isdigit(c) || std::isalpha(c)) {
                dstFileName << dstStreamName.Str()[i] << "/";
            }
            ++i;
        }

        TFsPath srcPath(srcFileName.Str());

        if (!srcPath.Exists())
            srcPath.MkDirs();

        srcFileName << srcStreamName.Str() << ".pcap";
        dstFileName << dstStreamName.Str() << ".pcap";

        TFsPath dstFilePath(dstFileName.Str());

        if (dstFilePath.Exists()) {
            DumperInstance_->AddDumpData(hashInv, dstFileName.Str(), pktHdr, packet);
        } else {
            DumperInstance_->AddDumpData(hash, srcFileName.Str(), pktHdr, packet);
        }
    }
}

static void ExitHandler(int signum) {
    Y_UNUSED(signum);

    CAPTURE_ENABLED.store(false, std::memory_order_relaxed);
}

static void SetExitHandler() {
    struct sigaction act {};

    act.sa_handler = ExitHandler;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGINT);
    sigaddset(&act.sa_mask, SIGTERM);

    sigaction(SIGINT, &act, NULL);
    sigaction(SIGTERM, &act, NULL);
}
