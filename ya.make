PROGRAM()

OWNER(g:balancer)

ALLOCATOR(LF)

PEERDIR(
    library/getopt
    contrib/libs/libpcap
    mapreduce/yt/client
    mapreduce/yt/util
)

SRCS(
    main.cpp
    splitter.cpp
)

END()
