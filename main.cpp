#include "main.h"

#include <library/getopt/opt.h>
#include <util/folder/path.h>

void printHelp(const char *name) {
    Cerr << STRINGBUF("Usage ") << name
         << STRINGBUF(":\n\t-r\tpath to pcap file\n")
         << STRINGBUF("*** File output:\n")
         << STRINGBUF("\t-o\toutput directory\n")
         << STRINGBUF("\t-d\tnumber of subdirectories level[0 - all in the same directory]\n")
         << STRINGBUF("*** YT output: \n")
         << STRINGBUF("\t-y\tuse YT as storage\n")
         << STRINGBUF("\t-t\ttable name\n")
         << STRINGBUF("\t-c\tcluster name[default: hahn]\n")
         << STRINGBUF("\t-a\tappend to existing table\n");

}

int RunMain(int argc, const char** argv) {
    NYT::Initialize(argc, argv);

    Opt opt(argc, argv, "r:o:d:t:c:yah");
    int optlet;

    TString filePath;
    TString outputDirectory;
    int depth = 0;

    bool ytUse = false;
    bool ytAppend = false;
    TString ytTableName;
    TString ytClusterName("hahn");

    while ((optlet = opt.Get()) != EOF) {
        switch (optlet) {
            case 'r': {
                filePath = ToString(opt.Arg);
                break;
            }
            case 'o': {
                outputDirectory = ToString(opt.Arg);
                break;
            }
            case 'd': {
                depth = FromString<int>(opt.Arg);
                break;
            }
            case 't': {
                ytTableName = ToString(opt.Arg);
                break;
            }
            case 'c': {
                ytClusterName = ToString(opt.Arg);
                break;
            }
            case 'y': {
                ytUse = true;
                break;
            }
            case 'a': {
                ytAppend = true;
                break;
            }
            default: {
                printHelp(argv[0]);
            }
        }
    }

    if (ytUse and !outputDirectory.Empty()) {
        Cerr << "Could not use YT and files at the same time\n";
        return 1;
    }

    if (filePath.Empty()) {
        Cerr << "Empty pcap file\n";
        return 1;
    }

    TFsPath pcapFile(filePath);

    if (!pcapFile.IsFile()) {
        Cerr << "Incorrect pcap file\n";
        return 1;
    }

    if (ytUse and ytTableName.Empty()) {
        Cerr << "Empty table name\n";
        return 1;
    }

    if (ytUse and ytClusterName.Empty()) {
        Cerr << "Empty cluster name\n";
        return 1;
    }

    if (argc == opt.Ind) {
        if (ytUse)
            return RunYtSplitter(filePath, ytClusterName, ytTableName, ytAppend);
        else
            return RunFileSplitter(filePath, outputDirectory, depth);
    } else {
        printHelp(argv[0]);
        return 1;
    }
}

int main(int argc, const char** argv) noexcept {
    return RunMain(argc, argv);
}
