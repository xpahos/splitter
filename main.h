#pragma once

#include <util/generic/string.h>
#include <util/generic/strbuf.h>
#include <util/string/cast.h>
#include <util/stream/output.h>
#include <util/stream/str.h>
#include <util/system/thread.h>

#include <contrib/libs/libpcap/pcap.h>
#include <mapreduce/yt/interface/client.h>

#include <vector>
#include <thread>

#if defined(_linux_)
#   include <sched.h>
#   include <pthread.h>
#endif
#include <arpa/inet.h>
#include <netinet/in.h>
#include <contrib/libs/libpcap/pcap-int.h>
#include "yt_dumper.h"

using TPcapPacketHeader = struct pcap_pkthdr;

/*
 * Standard libpcap format.
 */
constexpr uint32_t TCPDUMP_MAGIC = 0xa1b2c3d4;

/*
 * Normal libpcap format, except for seconds/nanoseconds timestamps,
 * as per a request by Ulf Lamping <ulf.lamping@web.de>
 */
constexpr uint32_t NSEC_TCPDUMP_MAGIC = 0xa1b23c4d;

class TFileDumper;
int RunMain(int argc, const char** argv);
int RunFileSplitter(const TString& filePath,
                    const TString& outputDir,
                    int depth);

int RunYtSplitter(const TString& filePath,
                  const TString& clusterName,
                  const TString& tableName,
                  bool append);


constexpr uint8_t IPV4 = 0x04;
constexpr uint8_t IPV6 = 0x06;
/* Basic optional fields */
constexpr uint8_t IP6OPT_TCP = 6;
constexpr uint8_t IP6OPT_HOP = 0;
constexpr uint8_t IP6OPT_ROUTING = 43;
constexpr uint8_t IP6OPT_FRAG = 44;
constexpr uint8_t IP6OPT_AH = 51;
constexpr uint8_t IP6OPT_DST = 60;
constexpr uint8_t IP6OPT_MIPV6 = 135;
constexpr uint8_t IP6OPT_SHIM6 = 140;

typedef struct {
    unsigned char ipVersion;
    unsigned char skipTOS;
    uint16_t skipIpLen;
    uint16_t skipIpId;
    uint16_t skipOffset;
    unsigned char skipIpTtl;
    unsigned char proto; // TCP 0x06
    uint16_t skipChecksum;
    struct in_addr srcIp;
    struct in_addr dstIp;
} TIp4Frame;

typedef struct {
    uint32_t skipVersionTrafficClassFlowLabel;
    uint16_t skipPayloadLen;
    unsigned char proto;
    unsigned char skipHopLimit;
    struct in6_addr srcIp;
    struct in6_addr dstIp;
} TIp6Frame;

typedef struct {
    unsigned char proto;
    unsigned char skipHdrExtLen;
    /* Skip all other optional fields */
} TIp6FrameNxt;

typedef struct {
    uint16_t srcPort;
    uint16_t dstPort;
    uint32_t seq;
    uint32_t ack;
    unsigned char skipOffset;
    unsigned char flags;
    uint16_t skipWindow;
    uint16_t skipCheckSum;
    uint16_t skipUrgent;
} TTcpFrame;

class TIpMultiFrame {
private:
    int AFInet_ = -1;
    uint8_t Proto_ = 0;
    unsigned char* SrcAdr_ = nullptr;
    unsigned char* DstAdr_ = nullptr;
public:
    void AFInet(int afInet) {
        AFInet_ = afInet;
    }

    int AFInet() const {
        return AFInet_;
    }

    void Proto(uint8_t proto) {
        Proto_ = proto;
    }

    uint8_t Proto() {
        return Proto_;
    }

    void SrcAdr(unsigned char* adr) {
        SrcAdr_ = adr;
    }

    unsigned char* SrcAdr() const {
        return SrcAdr_;
    }

    void DstAdr(unsigned char* adr) {
        DstAdr_ = adr;
    }

    unsigned char* DstAdr() const {
        return DstAdr_;
    }


};

class TPcapFilter {
private:
    struct bpf_program Filter_;
    TStringStream ErrorString_;
    bool IsInit_ = true;
public:
    TPcapFilter(pcap_t* session, const TString& filter) {
        if (pcap_compile(session, &Filter_, filter.c_str(), 0, 0) == -1) {
            ErrorString_ << STRINGBUF("Failed to parse filter [")
                 << filter << STRINGBUF("]: ")
                 << pcap_geterr(session)
                 << STRINGBUF("\n");
            IsInit_ = false;
        } else {
            if (pcap_setfilter(session, &Filter_) == -1) {
                ErrorString_ << STRINGBUF("Failed to install filter [")
                     << filter << STRINGBUF("]: ")
                     << pcap_geterr(session) << "\n";
                IsInit_ = false;
            }
        }
    }

    ~TPcapFilter() {
        pcap_freecode(&Filter_);
    }

    bool IsInit() const {
        return IsInit_;
    }

    TStringBuf ErrorString() {
        return ErrorString_.Str();
    }
};

class TPcap {
private:
    pcap_t* Session_ = nullptr;

    TStringStream ErrorString_;
    ptrdiff_t Offset_ = 0;
    bool init = true;

    char ErrorBuffer_[PCAP_ERRBUF_SIZE]{0};
public:
    TPcap(const TString& filePath) {
        Session_ = pcap_open_offline(filePath.c_str(), ErrorBuffer_);

        if (Session_ == nullptr) {
            ErrorString_ << STRINGBUF("Failed to open file [")
                 << filePath << STRINGBUF("]: ")
                 << ErrorBuffer_
                 << STRINGBUF("\n");
            init = false;
        } else {
            switch (LinkType()) {
                case DLT_NULL: // loopback bsd interface
                    Offset_ = 4;
                    break;
                case DLT_EN10MB: // eth interface
                    Offset_ = 14;
                    break;
                case DLT_LINUX_SLL: // Linux "any" interface
                    Offset_ = 16;
                    break;
                default:
                    ErrorString_ << STRINGBUF("Offset for link layer [")
                                 << LinkType()
                                 << STRINGBUF("] is not defined\n");
                    init = false;
            }
        }
    }

    ~TPcap() {
        if (Session_ != nullptr) {
            pcap_close(Session_);
        }
    }

    bool IsInit() {
        return init;
    }

    pcap_t* Session() {
        return Session_;
    }

    TPcapFileHeader FileHeader() const {
        TPcapFileHeader hdr;

        hdr.magic = Session_->opt.tstamp_precision == PCAP_TSTAMP_PRECISION_NANO ? NSEC_TCPDUMP_MAGIC : TCPDUMP_MAGIC;
        hdr.version_major = PCAP_VERSION_MAJOR;
        hdr.version_minor = PCAP_VERSION_MINOR;

        hdr.thiszone = Session_->tzoff;
        hdr.snaplen = Session_->snapshot;
        hdr.sigfigs = 0;
        hdr.linktype = Session_->linktype;
        hdr.linktype |= Session_->linktype_ext;

        return hdr;
    }

    ptrdiff_t Offset() const {
        return Offset_;
    }

    int NextEx(TPcapPacketHeader** pktHdr, const unsigned char** pkt) {
        int ret = pcap_next_ex(Session_, pktHdr, pkt);

        if (ret < 0) {
            ErrorString_.Clear();
            ErrorString_ << ToString(pcap_geterr(Session_));
        }

        return ret;
    }

    TStringBuf ErrorString() {
        return ErrorString_.Str();
    }

    int LinkType() const {
        return Session_->linktype;
    }

    int SnapSize() const {
        return Session_->snapshot;
    }
};

template<typename T>
class TClientWorker final {
private:
    std::unique_ptr<TPcap> Pcap_;
    std::unique_ptr<TPcapFilter> Filter_;
    std::unique_ptr<T> DumperInstance_;
    TString OutputDir_ = "";
    int Depth_ = 0;

    TClientWorker(
        std::unique_ptr<TPcap> pcap,
        std::unique_ptr<TPcapFilter> filter,
        std::unique_ptr<T> dumperInstance,
        const TString& outputDir,
        int depth)
        : Pcap_(std::move(pcap))
        , Filter_(std::move(filter))
        , DumperInstance_(std::move(dumperInstance))
        , OutputDir_(outputDir)
        , Depth_(depth)
    {};

    TClientWorker(
        std::unique_ptr<TPcap> pcap,
        std::unique_ptr<TPcapFilter> filter,
        std::unique_ptr<T> dumperInstance)
        : Pcap_(std::move(pcap))
        , Filter_(std::move(filter))
        , DumperInstance_(std::move(dumperInstance))
    {};

    void ProcessPacket(const TPcapPacketHeader *pktHdr, const unsigned char *packet);
    void EvalDump(const TPcapPacketHeader *pktHdr,
                  const unsigned char *packet,
                  const TTcpFrame* frame,
                  const unsigned char* srcIp,
                  const unsigned char* dstIp,
                  int afInet,
                  uint64_t hash,
                  uint64_t hashInv);
public:
    static std::unique_ptr<TClientWorker<T>> CreateWorker(const TString& filePath,
                                                const TString& outputDir,
                                                const TString& filter,
                                                int depth);
    static std::unique_ptr<TClientWorker<T>> CreateWorker(const TString &filePath,
                                                const TString &filter,
                                                NYT::ITransactionPtr ytClient,
                                                const TString &tableName);

    TClientWorker(const TClientWorker& that) = delete;
    TClientWorker& operator=(const TClientWorker& that) = delete;

    void Start();
};

