#pragma once

#include <unordered_map>
#include <util/folder/path.h>
#include "main.h"

constexpr size_t MRU_SIZE = 1000;

class TPcapDumper final {
private:
    pcap_t* Pd_;
    pcap_dumper_t* Dumper_;
public:
    TPcapDumper(const TString &fileName, int linkType, int snapSize) {
        TFsPath path(fileName);

        Pd_ = pcap_open_dead(linkType, snapSize);
        if (!path.Exists())
            Dumper_ = pcap_dump_open(Pd_, fileName.c_str());
        else
            Dumper_ = pcap_dump_open_append(Pd_, fileName.c_str());

        if (Dumper_ == nullptr) {
            Cerr << "PCAP Error: " << pcap_geterr(Pd_) << "\n";
            assert(0);
        }
    }

    ~TPcapDumper() {
        pcap_dump_flush(Dumper_);
        pcap_close(Pd_);
        pcap_dump_close(Dumper_);
    }

    void DumpData(const TPcapPacketHeader *pkthdr, const u_char *pkt) {
        pcap_dump((u_char *)Dumper_, pkthdr, pkt);
    }

};

class TFileDumper final {
private:
    class TMruNode final {
    private:
        TMruNode* Next_ = nullptr;
        TMruNode* Prev_ = nullptr;

        uint64_t Idx_ = 0;
        std::unique_ptr<TPcapDumper> Dumper_;
        TString FileName_;
    public:
        TMruNode(uint64_t idx, const TString &fileName, int linkType, int snapSize)
            : Idx_(idx)
            , FileName_(fileName)
        {
            Dumper_ = std::make_unique<TPcapDumper>(fileName, linkType, snapSize);
        }

        uint64_t Idx() const {
            return Idx_;
        }

        const TString& FileName() const {
            return FileName_;
        }

        TMruNode* Next() const {
            return Next_;
        }
        TMruNode* Prev() const {
            return Prev_;
        }

        void Next(TMruNode* node) {
            Next_ = node;
        }

        void Prev(TMruNode* node) {
            Prev_ = node;
        }

        void DumpData(const TPcapPacketHeader *pkthdr, const u_char *pkt) {
            Dumper_->DumpData(pkthdr, pkt);
        }
    };
private:
    std::unordered_map<uint64_t, TMruNode*> NodeMap_;
    TMruNode* Head_;
    TMruNode* Tail_;
    int LinkType_;
    int SnapSize_;
public:
    TFileDumper(int linkType, int snapSize)
        : Head_(nullptr)
        , Tail_(nullptr)
        , LinkType_(linkType)
        , SnapSize_(snapSize)
    {}

    ~TFileDumper() {
        TMruNode* curr;
        while (Head_ != nullptr) {
            curr = Head_;
            Head_ = Head_->Next();
            delete curr;
        }
    }

    bool HasKey(uint64_t idx) const {
        return NodeMap_.find(idx) != NodeMap_.end();
    }

    void DumpData(uint64_t idx, const TPcapPacketHeader *pkthdr, const u_char *pkt) {
        TMruNode* node = nullptr;

        auto it = NodeMap_.find(idx);
        if (it != NodeMap_.end()) {
            node = it->second;

            if (node->Prev() != nullptr) {
                node->Prev()->Next(node->Next());
                if (node->Next() != nullptr) {
                    node->Next()->Prev(node->Prev());
                } else {
                    Tail_ = node->Prev();
                }
                node->Prev(nullptr);
                node->Next(Head_);
                Head_->Prev(node);
            }

            Head_ = node;

            node->DumpData(pkthdr, pkt);
        } else {
            Cerr << "An expected index is not found. Undefined behavior.\n";
            assert(0);
        }
    }

    void AddDumpData(uint64_t idx, const TString& fileName, const TPcapPacketHeader *pkthdr, const u_char *pkt) {
        auto node = new TMruNode(idx, fileName, LinkType_, SnapSize_);

        if (Head_ != nullptr) {
            node->Next(Head_);
            Head_->Prev(node);
        } else {
            Tail_ = node;
        }
        NodeMap_[idx] = node;

        if (NodeMap_.size() >= MRU_SIZE) {
            TMruNode* last = Tail_->Prev();

            if (last != nullptr) {
                last->Next(nullptr);

                NodeMap_.erase(Tail_->Idx());
                if (NodeMap_.find(Tail_->Idx())==NodeMap_.end()) {
                }
                delete Tail_;
                Tail_ = last;
            }
        }

        Head_ = node;

        node->DumpData(pkthdr, pkt);
    }

    void Finish() {

    }
};
