#pragma once

#include "main.h"

#include <unordered_set>

using TPcapFileHeader = struct pcap_file_header;

class TYtDumper final {
private:
    bool init = true;
    TStringStream ErrorString_;
    NYT::TTableWriterPtr<NYT::TNode> YtWriter_ = nullptr;
    std::unordered_set<uint64_t> Set_;

    uint64_t OrderCounter_ = 0;

private:
    NYT::TNode CreateTableSchema() {
        NYT::TNode hash;
        hash["name"] = "hash";
        hash["type"] = "uint64";

        NYT::TNode order;
        order["name"] = "order";
        order["type"] = "uint64";

        NYT::TNode data;
        data["name"] = "data";
        data["type"] = "string";

        NYT::TNode schema;
        schema.Add(hash);
        schema.Add(order);
        schema.Add(data);

        return schema;
    }

    void CreatePcapHeader(NYT::ITransactionPtr ytClient, const TString& fileName, TPcapFileHeader fileHeader) {
        auto writer = ytClient->CreateFileWriter(fileName);
        writer->Write((void*)&fileHeader, sizeof(fileHeader));
        writer->Finish();
    }
public:
    TYtDumper(NYT::ITransactionPtr ytClient, const TString& tableName, TPcapFileHeader fileHeader) {
        TString fileName = tableName + "-header";

        if (ytClient->Exists(fileName)) {
            ErrorString_ << STRINGBUF("Headers file already exists\n");
            init = false;
            ytClient->Abort();
        } else {
            CreatePcapHeader(ytClient, fileName, fileHeader);

            if (ytClient->Exists(tableName)) {
                ErrorString_ << STRINGBUF("Temp table [") << tableName << STRINGBUF("] already exists\n");
                init = false;
                ytClient->Abort();
            } else {
                ytClient->Create(tableName, NYT::NT_TABLE,
                    NYT::TCreateOptions()
                        .IgnoreExisting(true)
                        .Recursive(true)
                        .Attributes(NYT::TNode()("schema", CreateTableSchema())("replication_factor", 1)));

                YtWriter_ = ytClient->CreateTableWriter<NYT::TNode>(tableName);
            }
        }
    }

    ~TYtDumper() {
        if (init) {
            YtWriter_->Finish();
        }
    }

    bool IsInit() const {
        return init;
    }

    TStringBuf ErrorString() const {
        return ErrorString_.Str();
    }

    void Finish() {
        if (init) {
            YtWriter_->Finish();
        }
    }

    void DumpData(uint64_t hash, uint64_t hashInv, const TString& pkt) {
        NYT::TNode node;

        if (Set_.find(hashInv) != Set_.end()) {
            Set_.insert(hashInv);
            node["hash"] = hashInv;
        } else {
            Set_.insert(hash);
            node["hash"] = hash;
        }

        node["data"] = pkt;
        node["order"] = OrderCounter_++;
        YtWriter_->AddRow(node);
    }
};
